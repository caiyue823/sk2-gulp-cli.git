# sk2-gulp-cli
[PROJECT HOME](https://gitee.com/caiyue823/sk2-gulp-cli)
## Intro
A workflow cli with built-in gulp-cli & gulpfile & flexible config

## Code Structure

```
└── sk2-gulp-cli ······project root
   ├─ bin ·············the cli runner for node
   │  ├─ index.js ·····the "bin" entry in package.json
   ├─ lib ·············the gulp relative files dir
   │  ├─ cmdPromise.js ····wrap node-cmd.run function into Promise style function exec
   │  ├─ config.js ····gulpfile build config, mainly for folder config
   │  ├─ data.js ······page display data config
   │  ├─ gulpfile.js ··the core gulpfile
   │  ├─ index.js ·····just simply export the gulpfile, also the "main" entry in package.json
   ├─ .eslintrc.js ····the default eslint config, can be override by project eslint config 
   ├─ .gitignore ······git ignore file
   ├─ .npmrc ··········mirror download links for npm libs
   ├─ LICENSE ········license file
   ├─ lint.yml ········not used currently
   ├─ package.json ····package config file
   ├─ project.config.js ····the project default config sample
   ├─ README.md ·······repo readme
   
```


## Manual

###Install
1.  `npm install sk2-gulp-cli -D` or `yarn add sk2-gulp-cli -D`
2.  `sk2-gulp-cli taskName`, check below for available tasks usage.

###CMD
### `sk2-gulp-cli lint`

Lint the styles & scripts files.

### `sk2-gulp-cli compile`

Compile the styles & scripts & pages file.

### `sk2-gulp-cli serve`

Runs the app in development mode with a automated server.

#### options

- `open`: Open browser on start, Default: `false`
- `port`: Specify server port, Default: `2080`

### `sk2-gulp-cli build`

Builds the app for production to the `dist` folder. It minify source in production mode for the best performance.

#### options

- `production`: Production mode flag, Default: `false`
- `prod`: Alias to `production`

### `sk2-gulp-cli start`

Running projects in production mode to view the actual result.
 Alias for `sk2-gulp-cli serve --prod`. 
In this mode, the file watch is disabled. If you want to watch the file changes, 
please use `sk2-gulp-cli serve` instead. 

#### options

- `open`: Open browser on start, Default: `false`
- `port`: Specify server port, Default: `2080`

### `sk2-gulp-cli deploy`

Deploy the `dist` folder to git.

#### options

- `branch`: The name of the branch you'll be pushing to, Default: `'gh-pages'`

### `sk2-gulp-cli clean`

Clean the `dist` & `temp` files.

### Config
You can overwrite config with a project.config.js in your project root dir.
A default config sample is like below. 
project.config.js is also available in the cli root dir for your convenience.
```javascript
module.exports = {
  data: {
    menus: [
      {
        name: 'Home',
        icon: 'aperture',
        link: 'index.html'
      },
      {
        name: 'Features',
        link: 'features.html'
      },
      {
        name: 'About',
        link: 'about.html'
      },
      {
        name: 'Contact',
        link: '#',
        children: [
          {
            name: 'Twitter',
            link: 'https://twitter.com/w_zce'
          },
          {
            name: 'About',
            link: 'https://weibo.com/zceme'
          },
          {
            name: 'divider'
          },
          {
            name: 'About',
            link: 'https://github.com/zce'
          }
        ]
      }
    ],
    pkg: require('./package.json'),
    date: new Date(),
  },
  config: {
    SRC: 'src',
    DIST: 'dist',
    TEMP: '.tmp',
    PUBLIC: 'public',
    PATHS: {
      style: 'assets/styles/*.scss',
      script: 'assets/scripts/*.js',
      page: '*.html',
      image: 'assets/images/**',
      font: 'assets/fonts/**',
    }
  }
}
```



