// 将cmd run方法包装成一个Promise方法exec
const cmd = require('node-cmd');

function exec(line) {
    return new Promise(function (resolve, reject) {
        cmd.run(line, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        })
    })
}

module.exports = {
    exec,
}