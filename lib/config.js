// 默认的config参数，可以被项目根目录的project.config.js里的config对象覆盖
module.exports = {
    SRC: 'src',
    DIST: 'dist',
    TEMP: 'temp',
    PUBLIC: 'public',
    PATHS: {
        style: 'assets/styles/*.scss',
        script: 'assets/scripts/*.js',
        page: '*.html',
        image: 'assets/images/**',
        font: 'assets/fonts/**',
    }
}