#!/usr/bin/env node

/**
 * 通过往命令行参数里添加--gulpfile，将运行项目的gulpfile指向当前cli包的lib/gulpfile
 * 通过往命令行参数里添加--cwd，并将运行项目的cwd传入，以防止gulp将运行目录重置为当前cli包目录
 */

process.argv.push('--gulpfile');
// require.resolve仅解析当前文件路径，并不执行当前文件代码，通过入参..的方式找到上一级目录（cli包目录）。
// 此时根据node查找模块的机制，会找到包目录的package.json里的main字段，从而定位到lib下的index文件
process.argv.push(require.resolve('..'));
process.argv.push('--cwd');
process.argv.push(process.cwd());
// console.log(process.argv);
// 这里使用了npm查找包的机制，会找到cli包下的node_modules里的gulp
// 而gulp中执行了require('gulp-cli')()，真正去执行gulpfile.js
require('gulp/bin/gulp');